showdown.setFlavor('github');
converter = new showdown.Converter({headerLevelStart: 3, noHeaderId: true});

function makeDate(d) {
  rd = "";
  rd += d[0];
  rd += "/";
  rd += d[1];
  rd += "/";
  rd += d[2];
  return rd
}

function makeByline(c) {
  ht = ""
  ht += '<div class="byline"><span class="by1">article by: </span><span class="by2">';
  ht += c.author;
  ht += '</span><span class="by1">published on: </span><span class="by2">';
  ht += makeDate(c.date);
  ht += '</span></div>'
  return ht
}

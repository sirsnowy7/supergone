function getQueryVariable(variable) { // https://css-tricks.com/snippets/javascript/get-url-variables/
       var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }
       return(false);
}

var file = getQueryVariable("article");
var dat;

$.get(file, function(data) {
  dat = data;
  $("#title > h2").append(dat.config.title);
  $("#title").after(makeByline(dat.config));
  $.get(dat.config.file, function(data) {
    md = data;
    md = converter.makeHtml(md);
    console.log(md)
    $("#body").append(md)
  })
  .fail(function(xhr) {
    console.error("ajax error: " + xhr.statusText);
  });
})
.fail(function(xhr) {
  console.error("ajax error: " + xhr.statusText);
});
// $("#title").after(byline)

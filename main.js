function addArticle(file) {
  file = "articles/" + file
  var dat;
  var html = "";
  var md = "";
  $.get(file, function(data) {
    dat = data;
    html += '<article><h2><a href="viewer.html?article=';
    html += file;
    html += '">'
    html += dat.config.title;
    html += '</a></h2>';
    html += makeByline(dat.config)
    html += '<div class="preview">';
    $.get(dat.config.file, function(data) {
      md = data;
      md = md.split("\n");
      md = md[0];
      md = converter.makeHtml(md);
      html += md;
      html += '</div></article>';
      $("#articles").append(html);
    })
    .fail(function() {
      console.error("ajax error: " + xhr.statusText);
    })
  })
  .fail(function() {
    console.error("ajax error: " + xhr.statusText);
  })
};

function listArticles(lst) {
  var dat;
  $.get(lst, function(data) {
    dat = data;
    for (var i = 0; i < dat.list.length; i++) {
      addArticle(dat.list[i]);
    };
  })
  .fail(function(xhr) {
    console.error("ajax error: " + xhr.status + " " + xhr.statusText);
  }, );
}

listArticles("articles/aalist.json");
